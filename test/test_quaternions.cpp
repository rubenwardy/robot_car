#include <Quaternion.hpp>
#include <unity.h>

void test_constructor() {
	Quaternion inst{1.f, V3(2.f, 3.f, 4.f)};
	TEST_ASSERT_EQUAL_FLOAT(1.f, inst.scalar);
}

void test_multiply() {
	Quaternion a{23.f, V3(54.f, 12.f, -4.f)};
	Quaternion b{-1.f, V3(23.f, 5.f, 7.f)};

	auto ret = a * b;
	TEST_ASSERT_EQUAL_FLOAT(-1297.f, ret.scalar);
	TEST_ASSERT_EQUAL_FLOAT(579.f, ret.vector.x);
	TEST_ASSERT_EQUAL_FLOAT(-367.f, ret.vector.y);
	TEST_ASSERT_EQUAL_FLOAT(159.f, ret.vector.z);
}

void test_rotations_yaw90cw() {
	auto yaw90 = Quaternion::rotateAround(V3(0, 0, 1), M_PI / 2.f);
	Serial.print(yaw90.scalar);
	Serial.print(", ");
	Serial.println(yaw90.vector);
	TEST_ASSERT_EQUAL_FLOAT(1.f, yaw90.length());

	const auto source = V3(3.f, 4.f, 2.f);
	auto rotated = yaw90.rotate(source);
	TEST_ASSERT_EQUAL_FLOAT(-4.f, rotated.x);
	TEST_ASSERT_EQUAL_FLOAT(3.f, rotated.y);
	TEST_ASSERT_EQUAL_FLOAT(2.f, rotated.z);

	rotated = yaw90.rotate(rotated);
	rotated = yaw90.rotate(rotated);
	rotated = yaw90.rotate(rotated);
	TEST_ASSERT_EQUAL_FLOAT(rotated.x, source.x);
	TEST_ASSERT_EQUAL_FLOAT(rotated.y, source.y);
	TEST_ASSERT_EQUAL_FLOAT(rotated.z, source.z);
}

void test_rotations_yaw90ccw() {
	auto yaw90 = Quaternion::rotateAround(V3(0, 0, 1), -M_PI / 2.f);
	Serial.print(yaw90.scalar);
	Serial.print(", ");
	Serial.println(yaw90.vector);
	TEST_ASSERT_EQUAL_FLOAT(1.f, yaw90.length());

	const auto source = V3(3.f, 4.f, 2.f);
	auto rotated = yaw90.rotate(source);
	TEST_ASSERT_EQUAL_FLOAT(4.f, rotated.x);
	TEST_ASSERT_EQUAL_FLOAT(-3.f, rotated.y);
	TEST_ASSERT_EQUAL_FLOAT(2.f, rotated.z);

	rotated = yaw90.rotate(rotated);
	rotated = yaw90.rotate(rotated);
	rotated = yaw90.rotate(rotated);
	TEST_ASSERT_EQUAL_FLOAT(rotated.x, source.x);
	TEST_ASSERT_EQUAL_FLOAT(rotated.y, source.y);
	TEST_ASSERT_EQUAL_FLOAT(rotated.z, source.z);
}


void run_quaternion_tests() {
    RUN_TEST(test_constructor);
	RUN_TEST(test_multiply);
	RUN_TEST(test_rotations_yaw90cw);
	RUN_TEST(test_rotations_yaw90ccw);
}
