#include <unity.h>
#include <nav.hpp>


void test_static_state() {
	State state{};
	IMUData imu = {
		gyro: V3(0, 0, 0),
		acc: V3(0, 0, 9.81f),
	};

	double delta = 0.01;
	for (int i = 0; i < 1000; i++) {
		update(state, delta, imu);
	}

	TEST_ASSERT_EQUAL_FLOAT(0.f, state.position.x);
	TEST_ASSERT_EQUAL_FLOAT(0.f, state.position.y);
	TEST_ASSERT_EQUAL_FLOAT(0.f, state.position.z);
	TEST_ASSERT_EQUAL_FLOAT(1.f, state.rotation.scalar);
	TEST_ASSERT_EQUAL_FLOAT(0.f, state.rotation.vector.x);
	TEST_ASSERT_EQUAL_FLOAT(0.f, state.rotation.vector.y);
	TEST_ASSERT_EQUAL_FLOAT(0.f, state.rotation.vector.z);
}


void run_nav_tests() {
	RUN_TEST(test_static_state);
}
