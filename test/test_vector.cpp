#include <Quaternion.hpp>
#include <unity.h>

void test_vector_dot() {
	V3 a{1, 2, 3};
	V3 b{4, -5, 6};

	TEST_ASSERT_EQUAL(12, a.dot(b));
}

void test_vector_cross() {
	V3 a{3, -3, 1};
	V3 b{4, 9, 2};
	V3 result = a.cross(b);

	TEST_ASSERT_EQUAL(-15, result.x);
	TEST_ASSERT_EQUAL(-2, result.y);
	TEST_ASSERT_EQUAL(39, result.z);
}

void run_vector_tests() {
	RUN_TEST(test_vector_dot);
	RUN_TEST(test_vector_cross);
}
