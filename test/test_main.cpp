#include <unity.h>

void run_vector_tests();
void run_quaternion_tests();
void run_nav_tests();

int main(int argc, char **argv) {
    UNITY_BEGIN();
	run_vector_tests();
	run_quaternion_tests();
    run_nav_tests();
    UNITY_END();

    return 0;
}
