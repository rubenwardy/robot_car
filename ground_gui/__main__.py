import math
from collections import namedtuple
from datetime import datetime
from typing import Tuple, List, Optional
import quaternion

import serial

import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib


plt.style.use('dark_background')
matplotlib.rcParams['figure.raise_window'] = False


RobotTelem = namedtuple("RobotTelem", "position velocity acceleration orientation")

def parse_telemetry(data: str) -> Optional[RobotTelem]:
	parts = data.split("|")
	if len(parts) != 5:
		return None

	orientation = [float(x) for x in parts[3].replace("rot", "").strip().split(",")]
	quat = np.quaternion(orientation[0], orientation[1], orientation[2], orientation[3])

	return RobotTelem(
			position=[float(x) for x in parts[1].replace("pos", "").strip().split(",")],
			velocity=[float(x) for x in parts[2].replace("vel", "").strip().split(",")],
			orientation=quat,
			acceleration=[float(x) for x in parts[4].replace("acc", "").strip().split(",")])


fig = plt.figure("Nav Test Platform")
plt.subplots_adjust(left=0.05, bottom=0.05, right=0.95, top=0.95)
# plt.subplots_adjust(wspace=0.01, hspace=0.01)
spec = fig.add_gridspec(ncols=3, nrows=2, width_ratios=[1, 4, 1], wspace=0.1, hspace=0.1)


class TimeSeries:
	def __init__(self, ylabel, gpos: Tuple[int, int]):
		self.ylabel = ylabel
		self.axes = fig.add_subplot(spec[gpos])
		self.axes.set_ylabel(self.ylabel)
		self.xs = []
		self.ys = []

	def push(self, value):
		self.xs.append(datetime.now().strftime('%H:%M:%S.%f'))
		self.ys.append(value)

		self.xs = self.xs[-20:]
		self.ys = self.ys[-20:]

		self.axes.clear()
		self.axes.set_ylabel(self.ylabel)
		self.axes.plot(self.xs, self.ys)
		self.axes.xaxis.set_ticklabels([])


graph_displacement = TimeSeries("Displacement / m", (0, 0))
graph_position = TimeSeries("Position / m", (1, 0))
graph_velocity = TimeSeries("Velocity / ms-1", (0, 2))
graph_acc = TimeSeries("Acceleration / ms-2", (1, 2))

map_ax = fig.add_subplot(spec[:, 1], projection="3d")
fig.add_axes(map_ax)
map_ax.autoscale(enable=True, axis='both', tight=True)





robot_marker = None
robot_vel = None



def update_robot(telem: RobotTelem):
	global robot_marker, robot_vel
	if robot_marker:
		robot_marker.remove()
		robot_vel.remove()

	R: quaternion.quaternion = telem.orientation
	facing = quaternion.as_vector_part(R * quaternion.from_vector_part([ 0, 1, 0 ]) * R.conjugate())

	robot_marker = map_ax.quiver(
		telem.position[0], telem.position[1], telem.position[2],
		facing[0], facing[1], facing[2],
		color="red", alpha=0.8, lw=3)

	robot_vel = map_ax.quiver(
		telem.position[0], telem.position[1], telem.position[2],
		telem.velocity[0]*5, telem.velocity[1]*4, telem.velocity[2]*5,
		color="green", alpha=0.8, lw=1)

	plt.draw()



# # # Setting the axes properties
map_ax.set_xlim3d([-5, 5])
map_ax.set_ylim3d([-5, 5])
map_ax.set_zlim3d([-5, 5])

robot_pos, = map_ax.plot3D([0], [0], [0])
plt.show(block=False)
plt.pause(1)


with serial.Serial("/dev/ttyACM1", timeout=None, baudrate=115200) as con:
	con: serial.Serial
	while con.isOpen():
		if con.readable() and con.inWaiting() > 0:
			try:
				line = con.readline().decode()
			except UnicodeDecodeError:
				print("Unicode error, skipping...")
				continue

			telem = parse_telemetry(line)
			if not telem:
				print(line)
				continue

			update_robot(telem)
			disp = math.sqrt(telem.position[0]**2 + telem.position[1]**2 + telem.position[2]**2)
			graph_displacement.push(disp)
			graph_position.push(telem.position)

			# spe = math.sqrt(telem.velocity[0]**2 + telem.velocity[1]**2 + telem.velocity[2]**2)
			graph_velocity.push(telem.velocity)

			# acc = math.sqrt(telem.acceleration[0]**2 + telem.acceleration[1]**2 + telem.acceleration[2]**2)
			graph_acc.push(telem.acceleration)

		plt.pause(0.01)
