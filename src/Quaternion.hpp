#pragma once
#include <math.h>
#include "Vector.hpp"
#include <Arduino.h>

class Quaternion {
public:
	double scalar;
	V3 vector;

	Quaternion(): scalar(1), vector() {}
	Quaternion(double scalar, V3 vector): scalar(scalar), vector(vector) {}

	static Quaternion rotateAround(V3 axis, double angle) {
		return { cos(angle / 2), axis * sin(angle / 2) };
	}

	inline double length() const {
		double sq = scalar*scalar + vector.sqlength();
		return sqrt(sq);
	}

	inline Quaternion normalized() const {
		double len = length();
		return { scalar / len, vector / len };
	}

	inline Quaternion operator*(const Quaternion &other) const {
		return {
			scalar * other.scalar - vector.dot(other.vector),
			other.vector * scalar + vector * other.scalar + vector.cross(other.vector)
		};
	}

	Quaternion &operator*=(const Quaternion &other) {
		*this = *this * other;
		return *this;
	}

	inline Quaternion conjugate() const {
		return { scalar, -vector };
	}

	V3 rotate(const V3 &source) const {
		return (*this * (Quaternion(0, source) * conjugate())).vector;
	}
};
