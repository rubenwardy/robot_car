#include <Arduino.h>
#include <Servo.h>
#include <SoftwareSerial.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BNO055.h>
#include <utility/imumaths.h>


#include "nav.hpp"

/**
 * Number of gaps = 20
 * Wheel circumference = 21.2cm
 */
const double WHEEL_GAP_TO_CM = 1.06;


class Wheel {
public:
	const int controlPin1;
	const int controlPin2;
	const int enablePin;

	Wheel(const int controlPin1,
			const int controlPin2,
			const int enablePin):
		controlPin1(controlPin1), controlPin2(controlPin2),
		enablePin(enablePin)
	{}

	void setup() {
		pinMode(controlPin1, OUTPUT);
		pinMode(controlPin2, OUTPUT);
		pinMode(enablePin, OUTPUT);

		digitalWrite(enablePin, LOW);
	}

	/**
	 * @param power From -255 to 255
	 */
	void setPower(int power) {
		if (power > 0) {
			digitalWrite(controlPin1, HIGH);
			digitalWrite(controlPin2, LOW);
		} else {
			digitalWrite(controlPin1, LOW);
			digitalWrite(controlPin2, HIGH);
		}

		analogWrite(enablePin, abs(power));
	}
};


class Button {
	const int pin;
	bool lastState = false;

public:
	explicit Button(int pin) : pin(pin) {}

	inline void setup() const {
		pinMode(pin, INPUT);
	}

	inline bool wasPressed() {
		bool current = digitalRead(pin) == HIGH;
		if (current != lastState) {
			lastState = current;
			return !current;
		}
		return false;
	}
};


Wheel leftWheel = {7, 8, 6};
Wheel rightWheel = {4, 9, 5};

Button stopButton{12};

int laserServoPort = 10;
Servo laserServo;
int laserAngle = 90;

SoftwareSerial MyBlue(2, 3); // RX | TX

int motorPower = 200;
int motorLeftSpeed = 0;
int motorRightSpeed = 0;

Adafruit_BNO055 bno = Adafruit_BNO055(55, 0x28);

State state{};

void printTelemetry(const State &state, const IMUData &imu) {
	// const double dist = state.position.length();
	// for (int i = 0; i <= 20; i++) {
	// 	if (abs(dist) > i * 0.1) {
	// 		Serial.print(F("#"));
	// 	} else if (i == 10) {
	// 		Serial.print(F("1"));
	// 	} else {
	// 		Serial.print(F(" "));
	// 	}
	// }
	// Serial.print(" | ");

	Serial.print(state.position.length());
	Serial.print(" | pos ");
	Serial.print(state.position);
	Serial.print(" | vel ");
	Serial.print(state.velocity);
	Serial.print(" | rot ");
	Serial.print(state.rotation.scalar);
	Serial.print(", ");
	Serial.print(state.rotation.vector);
	// Serial.print(" | gyro");
	// Serial.print(imu.gyro);
	Serial.print(" | acc ");
	Serial.println(state.acceleration_avg);
}

void displaySensorDetails(void)
{
	sensor_t sensor{};
	bno.getSensor(&sensor);
	Serial.println("------------------------------------");
	Serial.print  ("Sensor:       "); Serial.println(sensor.name);
	Serial.print  ("Driver Ver:   "); Serial.println(sensor.version);
	Serial.print  ("Unique ID:    "); Serial.println(sensor.sensor_id);
	Serial.print  ("Max Value:    "); Serial.print(sensor.max_value); Serial.println(" xxx");
	Serial.print  ("Min Value:    "); Serial.print(sensor.min_value); Serial.println(" xxx");
	Serial.print  ("Resolution:   "); Serial.print(sensor.resolution); Serial.println(" xxx");
	Serial.println("------------------------------------");
	Serial.println("");
	delay(500);
}

void setup() {
	Serial.begin(115200);
	Serial.println("Hello");

	MyBlue.begin(9600);

	// laserServo.attach(laserServoPort);
	// laserServo.write(laserAngle);

	stopButton.setup();
	leftWheel.setup();
	rightWheel.setup();

	// attachInterrupt(digitalPinToInterrupt(leftWheel.speedPin),
	// 	[]() { leftWheel.onEncoderTick(); }, RISING);
	// attachInterrupt(digitalPinToInterrupt(rightWheel.speedPin),
	// 	[]() { rightWheel.onEncoderTick(); }, RISING);

	pinMode(LED_BUILTIN, OUTPUT);
	pinMode(A0, INPUT);

	if (!bno.begin()) {
		Serial.println("Ooops, no BNO055 detected ... Check your wiring or I2C ADDR!");
		while (1);
	}

	bno.setSensorOffsets({
		accel_offset_x: 2,
		accel_offset_y: -12,
		accel_offset_z: -23,

		mag_offset_x: 229,
		mag_offset_y: -1315,
		mag_offset_z: 210,

		gyro_offset_x: 1,
		gyro_offset_y: -1,
		gyro_offset_z: 0,

		accel_radius: 1000,
		mag_radius: 896
	});

	delay(1000);
	bno.setExtCrystalUse(true);
	displaySensorDetails();
}

void handleCommand(const char &cmd) {
	switch (cmd) {
	case 'q':
		motorLeftSpeed = 0;
		motorRightSpeed = 0;
		break;
	case 'w':
		motorLeftSpeed = motorPower;
		motorRightSpeed = motorPower;
		break;
	case 'a':
		motorLeftSpeed = -motorPower;
		motorRightSpeed = motorPower;
		break;
	case 'd':
		motorLeftSpeed = motorPower;
		motorRightSpeed = -motorPower;
		break;
	case 's':
		motorLeftSpeed = -motorPower;
		motorRightSpeed = -motorPower;
		break;
	case 'o':
		laserAngle = max(laserAngle - 1, 0);
		// laserServo.write(laserAngle);
		break;
	case 'p':
		laserAngle = 179;//min(laserAngle + 1, 179);
		// laserServo.write(laserAngle);
		break;
	default:
		Serial.print("Unknown command: ");
		Serial.println(cmd);
		break;
	}
}

int counter = 0;

void loop() {
	delay(10);

	if (Serial.available()) {
		int cmd = Serial.read();
		handleCommand(cmd);
	}

	if (stopButton.wasPressed()) {
		handleCommand('q');
	}

	leftWheel.setPower(motorLeftSpeed);
	rightWheel.setPower(motorRightSpeed);

	if (MyBlue.available()) {
		int cmd = MyBlue.read();
		handleCommand(cmd);
	}

	imu::Quaternion quat = bno.getQuat();
	auto acc = bno.getVector(Adafruit_BNO055::VECTOR_LINEARACCEL);
	IMUData imuData = {
		orientation: Quaternion(quat.w(), V3(quat.x(), quat.y(), quat.z())),
		acc: V3(acc.x(), acc.y(), acc.z()),
	};

	update(state, 0.01, imuData);

	counter++;
	if (counter > 10) {
		printTelemetry(state, imuData);
		counter = 0;
	}
}
