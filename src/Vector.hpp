#pragma once

#include <math.h>
#include <Print.h>
#include <Printable.h>

template <class T = double>
class Vector3 : public Printable {
public:
	T x, y, z;

	Vector3(): x(0), y(0), z(0) {}
	Vector3(T x, T y, T z): x(x), y(y), z(z) {}
	Vector3(const Vector3 &other): x(other.x), y(other.y), z(other.z) {}

	Vector3<T> &operator+=(const Vector3<T> &other) {
		x += other.x;
		y += other.y;
		z += other.z;
		return *this;
	}

	Vector3<T> &operator-=(const Vector3<T> &other) {
		x -= other.x;
		y -= other.y;
		z -= other.z;
		return *this;
	}

	Vector3<T> operator-() const {
		return { -x, -y, -z };
	}

	Vector3<T> operator+(const Vector3<T> &other) const {
		return Vector3(x + other.x, y + other.y, z + other.z);
	}

	Vector3<T> operator-(const Vector3<T> &other) const {
		return Vector3(x - other.x, y - other.y, z - other.z);
	}

	Vector3<T> operator*(T scalar) const {
		return Vector3(x * scalar, y * scalar, z * scalar);
	}

	Vector3<T> operator/(T scalar) const {
		return Vector3(x / scalar, y / scalar, z / scalar);
	}

	T dot(const Vector3<T> &other) const {
		return x * other.x + y * other.y + z * other.z;
	}

	Vector3<T> cross(const Vector3<T> &other) const {
		return {
			y * other.z - z * other.y,
			z * other.x - x * other.z,
			x * other.y - y * other.x
		};
	}

	inline double sqlength() const {
		return x * x + y * y + z * z;
	}

	inline double length() const {
		return sqrt(sqlength());
	}

	Vector3<T> normalized() const {
		return *this / length();
	}

	size_t printTo(Print& p) const override {
		size_t r = 0;
		r += p.print(x);
		r += p.print(", ");
		r += p.print(y);
		r += p.print(", ");
		r += p.print(z);
		return r;
	}
};

using V3 = Vector3<double>;
