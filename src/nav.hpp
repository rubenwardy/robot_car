#pragma once
#include <math.h>
#include "Vector.hpp"
#include "Quaternion.hpp"
#include <RingBuf.h>

const double GRAVITY = 9.81;


struct State {
	V3 position;
	V3 velocity;
	RingBuf<V3, 10> acceleration;
	V3 acceleration_avg;
	Quaternion rotation;
};


struct IMUData {
	Quaternion orientation;
	// V3 gyro;
	V3 acc;
};


Quaternion gyroToRotationQuaternion(V3 gyro, double delta) {
	double length = gyro.length();
	if (length == 0) {
		return {};
	}

	return Quaternion::rotateAround(gyro / length, delta * length);
}


void update(State &state, double delta, const IMUData &imu) {
	// state.rotation *= gyroToRotationQuaternion(imu.gyro, delta);
	state.rotation = imu.orientation;

	// Update average
	if (state.acceleration.isFull()) {
		V3 dropping_out;
		state.acceleration.pop(dropping_out);
		state.acceleration_avg += (imu.acc - dropping_out) / (double)state.acceleration.maxSize();
	} else {
		state.acceleration_avg += (imu.acc - state.acceleration_avg) / ((double)state.acceleration.size() + 1.0);
	}
	state.acceleration.push(imu.acc);

	// {
	// 	V3 sum;
	// 	for (uint8_t j = 0; j < state.acceleration.size(); j++) {
	// 		sum += state.acceleration[j];
	// 	}

	// 	V3 avg = sum / (double)state.acceleration.size();

	// 	Serial.print(F(" expected = "));
	// 		Serial.print(avg);
	// 		Serial.print(F(" actual = "));
	// 		Serial.println(state.acceleration_avg);

	// 	if ((avg - state.acceleration_avg).length() > 0.001f) {
	// 		Serial.print(F("Bad acceleration calculation "));
	// 		Serial.print(F(" expected = "));
	// 		Serial.print(avg);
	// 		Serial.print(F(" actual = "));
	// 		Serial.println(state.acceleration_avg);
	// 		for (uint8_t j = 0; j < state.acceleration.size(); j++) {
	// 			Serial.println(state.acceleration[j]);
	// 		}
	// 		while (1) { delay(1000); }
	// 	}
	// }

	state.position += state.velocity * delta + state.acceleration_avg * 0.5 * delta * delta;
	state.velocity += state.acceleration_avg * delta;
}
